import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import YTSearch from 'youtube-api-search';
import SearchBar from './components/search_bar';
import VideoList from './components/video_list';
import VideoDetail from './components/video_detail'
import _ from 'lodash';
const API_KEY = 'AIzaSyBKyjxG_HLLP5PAlESDGeSWgK0SCM37NBc'

class App extends Component  {
  constructor(props) {
    super(props);
    this.state = {
      videos: [],
      selectedVideo: null
    };
    this.videoSearch('shibainu puppie')
  }

  videoSearch(term) {
    YTSearch({ key: API_KEY, term: term }, (videos) => {
      this.setState(
        Object.assign({},this.state,{ selectedVideo: videos[0], videos: videos })
      );
    });

  }

  render () {
    const videoDSearch = _.debounce((term) => { this.videoSearch(term) } ,300);

    return (
      <div>
        <SearchBar onSearchTermChange={videoDSearch} />
        <VideoDetail video={this.state.selectedVideo} />
        <VideoList
          onVideoSelect={selectedVideo => this.setState({selectedVideo})}
          videos={this.state.videos} />
      </div>
      );
   }
}

let node = document.querySelector('.container')

ReactDOM.render(<App />, node);
